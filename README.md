# colorizer web project
## Voraussetzungen
Python > 3.6
## Anleitung zum lokalen Betrieb

1. In Projektordner wechseln

2. Virtualenv einrichten

`python3 -m venv <virtualenvname>`

3. Virtualenv einschalten

`source <virtualenvname>/bin/activate`

4. Abhängigkeiten installieren

`pip install -r requirements.txt`

5. Projekt starten

`python3 app.py`

